import imageio as iio
import itertools as it
from PIL import Image
import random
import string
from pygifsicle import optimize

BACKGROUND = "./assets/soup/soup0.jpg"
ALPHABET_DIR = "./assets/alphabet/"
WORD_LIST = "./assets/wordlist/wordlist.txt"

LETTER_1_COORDS = (395, range(279, 500))
LETTER_2_COORDS = (595, range(279, 500))
LETTER_3_COORDS = (800, range(279, 500))


def generate_soup(letter1, letter2, letter3):
    background = Image.fromarray(iio.imread(BACKGROUND))
    letter1_img = iio.get_reader(f'{ALPHABET_DIR}{letter1}.gif')
    letter2_img = iio.get_reader(f'{ALPHABET_DIR}{letter2}.gif')
    letter3_img = iio.get_reader(f'{ALPHABET_DIR}{letter3}.gif')

    result_frames = []

    l1_coords = (LETTER_1_COORDS[0], random.choice(LETTER_1_COORDS[1]))
    l2_coords = (LETTER_2_COORDS[0], random.choice(LETTER_2_COORDS[1]))
    l3_coords = (LETTER_3_COORDS[0], random.choice(LETTER_3_COORDS[1]))
    for frame1, frame2, frame3 in zip(letter1_img, letter2_img, letter3_img):
        background_frame = background.copy()
        img1_frame = Image.fromarray(frame1)
        img2_frame = Image.fromarray(frame2)
        img3_frame = Image.fromarray(frame3)
        background_frame.paste(img1_frame, l1_coords, img1_frame)
        background_frame.paste(img2_frame, l2_coords, img2_frame)
        background_frame.paste(img3_frame, l3_coords, img3_frame)
        result_frames.append(background_frame)
    out = f'./results/{letter1}{letter2}{letter3}.gif'
    iio.mimsave(out, result_frames)
    # optimize(out)


def main():
    start_from = None

    skip = True
    # everything = it.combinations(string.ascii_lowercase, 3)
    wordlist = open(WORD_LIST, 'r').read().lower().split(' ')
    length = len(wordlist)
    # for i, subset in enumerate(everything):
    for i, subset in enumerate(wordlist):
        if skip:
            if start_from is not None and ''.join(subset) != start_from:
                continue
            else:
                skip = False

        generate_soup(*subset)

        print(f'[{i+1}/{length}]  generated {subset}')


if __name__ == '__main__':
    main()
