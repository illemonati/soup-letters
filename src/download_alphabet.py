import shutil
import requests
import string


def download_alphabet():
    alphabet = string.ascii_lowercase
    for letter in alphabet:
        url = f'http://dance.cavifax.com/images/{letter}.gif'
        resp = requests.get(url, stream=True)
        with open(f'./assets/alphabet/{letter}.gif', 'wb+') as out:
            shutil.copyfileobj(resp.raw, out)
        del resp
        print(f'{letter} downloaded')


if __name__ == '__main__':
    download_alphabet()
